const express = require('express');
const bodyParser = require('body-parser');
const paint = require('./paint');
const app = express();

// App config
app.use(bodyParser.urlencoded({extended: false}));
app.set('view engine', 'ejs');
app.use(express.static('public'));

// Routes
app.get('/', (req, res) => {
  const matrix = {
    width: 0,
    height: 0,
    grid: [],
  };

  res.render('index', {matrix: matrix});
});

app.post('/', (req, res) => {
  const matrix = {};

  // Get the height and the width from client
  matrix.height = req.body.height;
  matrix.width = req.body.width;

  // Get the picked colors from client
  const colors = [];
  colors.push(req.body.color1);
  colors.push(req.body.color2);
  colors.push(req.body.color3);

  // Call function from the paint module to generate the matrix
  matrix.grid = paint.generateRandomGrid(matrix.height, matrix.width, colors);

  res.render('index', {matrix: matrix});
});


app.post('/floodfill', (req, res) => {
  // Initialize variables
  const newMatrix = {};
  const gridFromClient = JSON.parse(req.body.grid);
  const cell = JSON.parse(req.body.cell);
  const color = req.body.color;

  // Check if the clicked cell is already the same color as the picked one,
  // if not, call the floodFillAt function
  if (gridFromClient[cell.clicked_row][cell.clicked_column] !== color) {
    newMatrix.grid = paint.floodFillAt(gridFromClient, cell.clicked_column,
        cell.clicked_row, color);
  } else {
    newMatrix.grid = gridFromClient;
  }

  newMatrix.height = newMatrix.grid.length;
  newMatrix.width = newMatrix.grid[0].length;

  res.send(newMatrix);
});

app.listen(3000, () =>
  console.log('Server is listening!'),
);
