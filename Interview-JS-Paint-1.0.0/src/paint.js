// lodash
const _ = require('lodash');

/**
 * Fills all adjecent cells that are the same color as the cell at the y row
 * and x column of the grid with the new color
 *
 * @param {Array} grid The grid on which you should implement the flooding
 * algorithm using the color defined at the x and y coordinates given in the
 * x and y args
 * @param {number} x The x coordinate
 * @param {number} y The y coordinate
 * @param {string} newColor The color to fill with using the flood algorithm
 * @return {Array} The grid array with the implemented flood fill algorithm
 */
function floodFillAt(grid, x, y, newColor) {
  const originColor = grid[y][x];
  recursiveColoring(grid, x, y, originColor, newColor);
  return grid;
}

exports.floodFillAt = floodFillAt;


/**
 * Fetches all adjacent cells that are the same color as the given cell
 *
 * @param {Array} grid The grid representing the image
 * @param {number} x The x coordinate (column)
 * @param {number} y The y coordinate (row)
 * @param {string} originColor The color from which we started flood fill
 * @return {Array} A list of neighbors with originColor
 */
function getAllSameColorNeighbors(grid, x, y, originColor) {
  const listOfNeighbors = [];
  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      if ((typeof grid[y + i] == 'undefined') ||
          (typeof grid[y + i][x + j] == 'undefined')) {
        continue;
      } else {
        if (grid[y + i][x + j] === originColor) {
          listOfNeighbors.push([y + i, x + j]);
        }
      }
    }
  }
  return listOfNeighbors;
}

/**
 * Colors all adjacent cells recursively
 *
 * @param {Array} grid The grid representing the image
 * @param {number} x The x coordinate (column) from which to paint
 * @param {number} y The y coordinate (row) from which to paint
 * @param {string} originColor The color from which we started flood fill
 * @param {string} newColor The color to fill
 */
function recursiveColoring(grid, x, y, originColor, newColor) {
  grid[y][x] = newColor;
  const sameColorNeighbors = getAllSameColorNeighbors(grid, x, y, originColor);

  if (sameColorNeighbors.length == 0) {
    return;
  } else {
    for (let index = 0; index < sameColorNeighbors.length; index++) {
      const x1 = sameColorNeighbors[index][1];
      const y1 = sameColorNeighbors[index][0];

      recursiveColoring(grid, x1, y1, originColor, newColor);
    }
  }
}


/**
 * Generates a (rowsxcolumns) grid where the color of each point in the grid
 * is randomly selected to be one of the three colors the user picked
 *
 * @param {number} rows Number of rows
 * @param {number} columns Number of columns
 * @param {Array} colors Array of colors to use
 * @return {Array} Randomly generated 2D matrix
 */
function generateRandomGrid(rows, columns, colors) {
  // The colors this grid allows
  const COLORS = colors;

  // The number of columns in this grid
  const GRID_NUMBER_COLUMNS = columns;
  // The number of rows in this grid
  const GRID_NUMBER_ROWS = rows;

  // The 2D array which will be used to store the randomly
  // generated color values
  const grid = [];

  // Move row by row and populate each point in the row with a random color
  for (let rowIndex = 0; rowIndex < GRID_NUMBER_ROWS; rowIndex++) {
    // Create the array at this row which represents the column
    grid[rowIndex] = [];

    // Go through each point in the column
    for (let columnIndex = 0;
      columnIndex < GRID_NUMBER_COLUMNS;
      columnIndex++) {
      // Generate the random color for the point at rowIndex,columnIndex
      const colorForCurrentCoord = COLORS[_.random(0, 2)];

      // Set the color
      grid[rowIndex][columnIndex] = colorForCurrentCoord;
    }
  }

  // Return the generated grid
  return grid;
}
exports.generateRandomGrid = generateRandomGrid;
