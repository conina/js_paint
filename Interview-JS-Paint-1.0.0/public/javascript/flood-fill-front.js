const CELL_SIZE = 30;

// Do the first draw on canvas
drawGrid(matrixObj);

/**
 * Draw a grid on canvas
 * @param {Array} matrix 2D array of colors to draw
 */
function drawGrid(matrix) {
  const canvas = $('#my_canvas').get(0);
  const ctx = canvas.getContext('2d');
  let row = 0;
  let column = 0;

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  // Going through each row, fill the cell in each column
  for (let i = 0; i < matrix.height; i++) {
    for (let j = 0; j < matrix.width; j++) {
      ctx.fillStyle = matrix.grid[i][j];
      ctx.fillRect(column, row, CELL_SIZE, CELL_SIZE);
      column = column + CELL_SIZE;
    }

    column = 0;
    row = row + CELL_SIZE;
  }
}

// Change the color when the color in the colorPicker is changed
// let newColor = document.getElementById('newColor').value;
let newColor = $('#new_color').val();
$('#new_color').on('change', function() {
  newColor = $(this).val();
});

// Send the the new color, the column, and the row to the server
// to update the grid
$('#my_canvas').click((evt) => {
  const c = $('#my_canvas').get(0); ;
  const mousePosition = getMousePos(c, evt);

  // Get the column and the row position by dividing coordinates
  // with the cell size
  const clickedCell = {};
  clickedCell.clicked_column = Math.floor(mousePosition.mouse_x / CELL_SIZE);
  clickedCell.clicked_row = Math.floor(mousePosition.mouse_y / CELL_SIZE);

  // Ajax call to the server with the new matrix information
  $.ajax({
    url: 'floodfill/',
    type: 'POST',
    data: {
      'grid': JSON.stringify(matrixObj.grid),
      'cell': JSON.stringify(clickedCell),
      'color': newColor,
    },
    success: function(result) {
      matrixObj.grid = result.grid;
      drawGrid(matrixObj);
    },
  });
});

/**
 * Get the mouse position
 *
 * @param {*} canvas Canvas element
 * @param {*} evt Mouse click event
 * @return {Object} Object with mouse_x and mouse_y properties
 */
function getMousePos(canvas, evt) {
  const rect = canvas.getBoundingClientRect();
  return {
    mouse_x: evt.clientX - rect.left,
    mouse_y: evt.clientY - rect.top,
  };
}
